<?php

namespace Bentleyworks\Revel\Controllers\Http;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

class RevelController extends Controller
{
    private $request;
    
    public function __construct(\Request $request)
    {
        $this->request = $request;
    }
    
    public function revel()
    {
        Inertia::setRootView('bentleyworks::app');
        
        $dbs = \Redis::config('get', 'databases');
        $t = 1299;
        return Inertia::render('Revel', [
            'dbs' => $dbs,
        ]);
        
    }

}
