<?php

namespace Bentleyworks\Revel;

use Illuminate\Support\ServiceProvider;

class RevelServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'bentleyworks');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'bentleyworks');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        
        $this->mergeConfigFrom(__DIR__.'/../config/revel.php', 'revel');

        
        /*
        $this->publishes([
            '/Users/pedro/PhpstormProjects/sixacts/packages/Bentleyworks/Revel/resources/views' =>
                resource_path('views/vendor/revel'),
        ]);*/
    
        $this->publishes([
            __DIR__.'/../resources' => public_path('vendor/revel'),
        ], 'public');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/revel.php', 'revel');
 
        // Register the service the package provides.
        $this->app->singleton('revel', function ($app) {
            return new Revel;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['revel'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/revel.php' => config_path('revel.php'),
        ], 'revel.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/bentleyworks'),
        ], 'revel.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/bentleyworks'),
        ], 'revel.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/bentleyworks'),
        ], 'revel.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
