<?php

namespace Bentleyworks\Revel\Facades;

use Illuminate\Support\Facades\Facade;

class Revel extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'revel';
    }
}
