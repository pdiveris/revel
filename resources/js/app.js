require('./bootstrap');
window.Vue = require('vue');

Vue.config.devtools = true;
Vue.config.performance = true;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import { InertiaApp } from '@inertiajs/inertia-vue';
import Vue from 'vue';

Vue.use(InertiaApp);

const app = document.getElementById('app');

const files = require.context('./', true, /\.vue$/i);

new Vue({
    render: h => h(InertiaApp, {
        props: {
            initialPage: JSON.parse(app.dataset.page),
            resolveComponent: page => files(`./components/Pages/${page}.vue`).default,
        },
    }),
}).$mount(app);
