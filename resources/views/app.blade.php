<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="{{ asset('/vendor/bentleyworks/css/app.css') }}" rel="stylesheet">
    <script async type="text/javascript" src="{{asset('vendor/bentleyworks/js/app.js')}}"></script>
</head>
<body>
    {{ var_dump($page['props']['dbs']) }}
   <div id="app" data-page="{{ json_encode($page) }}" :data="{ foo: bar }"></div>
</body>
</html>

