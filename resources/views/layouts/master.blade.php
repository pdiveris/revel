<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Kube CSS -->
    <link href="{{ asset('/vendor/bentleyworks/css/app.css') }}" rel="stylesheet">
    <script async type="text/javascript" src="{{asset('vendor/bentleyworks/js/app.js')}}"></script>
</head>
<body>
<body>
<div class="page is-inset-x-32 is-inset-y-8">
    <header class="header">
        <div class="is-navbar-box">
            <div class="is-brand">
                <b>Brand</b>
                <a href="#" class="icon-kube-menu is-shown-mobile"
                   data-kube="toggle" data-target="#my-navbar">
                </a>
            </div>
            <div id="my-navbar" class="is-navbar is-hidden-mobile">
                <nav class="is-stacked is-push-right">
                    <ul>
                        <li><a href="#">one</a></li>
                        <li><a href="#">two</a></li>
                        <li><a href="#">three</a></li>
                        <li><a href="#">four</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <main class="main is-container">
        @yield('content')
    </main>
    <footer class="footer">
        FOOTER
    </footer>
</div>
</body>



</body>
</html>
